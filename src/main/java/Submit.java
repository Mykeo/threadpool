
import java.util.List;
import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * Accepts runnable, than proceeds to execute them.
 */
public class Submit {

    private final PriorityQueue<TimelyRunnable> submissions;
    private final List<Thread> threads;

    /**
     * Creates a producer with fixed list of consumers.
     * @param processCount How many consumers to create.
     */
    public Submit(int processCount) {

        submissions = new PriorityQueue<>(TimelyRunnable::compareTo);
        threads = new ArrayList<>(processCount);

        while (processCount-- > 0)
            threads.add(new Thread(
                    new Process(submissions))
            );

        threads.forEach(Thread::start);
    }

    /**
     * Accepts another task.
     * @param task A runnable instance.
     */
    public void submit(Runnable task) {
        long noTimeout = 0;
        submit(task, noTimeout);
    }

    /**
     * Accepts runnable task with a timeout.
     * @param task A runnable instance.
     * @param timeout The time thread sleeps until goes to action.
     */
    public void submit(Runnable task, long timeout) {
        synchronized (submissions) {
            submissions.add(boundRunnableToTime(timeout, task));
            submissions.notifyAll();
        }
    }

    /**
     * Shuts down every process.
     * Ceases execution when each Process is finished.
     */
    public void shutdown() {
        threads.forEach(Thread::interrupt);
    }

    /**
     * Bound runnable to its execution time.
     * The execution time is the current time plus offset.
     * @param task A runnable instance.
     * @param offset Time in milliseconds.
     * @return A runnable with offset.
     */
    private TimelyRunnable boundRunnableToTime(long offset, Runnable task) {
        long currently = System.currentTimeMillis();
        return new TimelyRunnable(currently + offset, task);
    }
}
