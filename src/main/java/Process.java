import java.util.PriorityQueue;

/**
 * A process represents another thread that awaits opportunity to run another Runnable.
 */
class Process implements Runnable {

    private final PriorityQueue<TimelyRunnable> submissions;

    /**
     * Queue that accepts timely submissions.
     * @param submissions Priority queue of timely runnables.
     */
    public Process(PriorityQueue<TimelyRunnable> submissions) {
        this.submissions = submissions;
    }

    /**
     * Whether there is any runnable awaiting execution.
     * @return Presence of a ready timely runnable.
     */
    private boolean containsReadyRunnable() {
        if (!submissions.isEmpty()) {
            return submissions.peek().getStartTime() <= System.currentTimeMillis();
        }
        return false;
    }

    /**
     * Shows whether there is any task at all.
     * @return Is there is any runnable.
     */
    private boolean containsRunnable() {
        return !submissions.isEmpty();
    }

    /**
     * Returns how much time does a process needs to wait to accept a new timely runnable.
     * @param task A timely runnable.
     * @return How much time a process needs to wait to reach the task's execution time.
     */
    private long waitingTime(TimelyRunnable task) {
        long executionTime = task.getStartTime();
        long callTime = System.currentTimeMillis();
        long waitingTime = executionTime - callTime;
        return waitingTime > 0 ? waitingTime : 0;
    }

    /**
     * Suspends execution until any executable submission appears.
     * This involves that the queue must not be empty, otherwise it suspends execution.
     * The process will resume only when a new ready runnable will be offered.
     * Note, that in case the runnable is not ready for execution yet,
     * the process will suspend its work until the runnable becomes ready.
     * @return "true" when process is interrupted,
     * "false" otherwise.
     */
    private boolean suspendUntilReadyRunnableAppears() {
        while (!containsReadyRunnable()) {
            try {
                if (containsRunnable()) {
                    var awaitingRunnable = submissions.peek();

                    // containsRunnable guarantees that awaitingRunnable is not null;
                    long suspension = waitingTime(awaitingRunnable);
                    submissions.wait(suspension);
                }
                else
                    submissions.wait();
            } catch (InterruptedException ignored) {
                return true;
            }
        }
        return false;
    }

    /**
     * Runs process to consume any incoming Runnable unless submission queue is empty.
     */
    public void run() {
        var currentThread = Thread.currentThread();
        while (!currentThread.isInterrupted()) {
            Runnable task;
            synchronized (submissions) {

                // Guarantees that task is not null;
                if (suspendUntilReadyRunnableAppears())
                    break;

                task = submissions.poll();
                submissions.notifyAll();
            }
            task.run();
        }
        currentThread.interrupt();
    }
}
