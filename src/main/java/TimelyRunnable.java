
/**
 * Ties a Runnable to time.
 */
public class TimelyRunnable implements Runnable, Comparable<TimelyRunnable> {

    private final long startTime;
    private final Runnable task;

    /**
     * Creates a TimelyRunnable runnable.
     * @param startTime Time to start in milliseconds.
     * @param task A Runnable instance.
     */
    public TimelyRunnable(long startTime, Runnable task) {
        this.startTime = startTime;
        this.task = task;
    }

    /**
     * What time should a runnable start.
     * @return Time when execution is prompted. Time is in milliseconds.
     */
    public long getStartTime() { return startTime; }

    /**
     * Compares two TimelyRunnable-s on the time scale.
     * @param another Another instance of TimelyRunnable.
     * @return Relation in time between current TimelyRunnable instance and the other.
     */
    @Override
    public int compareTo(TimelyRunnable another) {
        long diff = startTime - another.startTime;
        if (diff > 0) return 1;
        else if (diff == 0) return 0;
        else return -1;
    }

    /**
     * Helps running the Runnable instance kept within.
     */
    @Override
    public void run() { task.run(); }

    @Override
    public String toString() {
        return "TimelyRunnable{" +
                "startTime=" + startTime +
                '}';
    }
}
