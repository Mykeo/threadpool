import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubmitTest {

    private static Runnable run500ms;
    private static Runnable run3000ms;
    private static Runnable runTimely1000ms;
    private static Runnable runTimely2000ms;
    private static Runnable runTimely4000ms;

    private static List<Runnable> competitionOrder;


    @BeforeEach
    public void updateSubmissions() {

        competitionOrder = new ArrayList<>();

        run500ms = () -> {
            try {
                Thread.sleep(500);
                synchronized (competitionOrder) {
                    competitionOrder.add(run500ms);
                }
            } catch (InterruptedException ignored) { }
        };
        run3000ms = () -> {
            try {
                Thread.sleep(3000);
                synchronized (competitionOrder) {
                    competitionOrder.add(run3000ms);
                }
            } catch (InterruptedException ignored) { }
        };


        runTimely1000ms = () -> {
            synchronized (competitionOrder) {
                competitionOrder.add(runTimely1000ms);
            }
        };
        runTimely2000ms = () -> {
            synchronized (competitionOrder) {
                competitionOrder.add(runTimely2000ms);
            }
        };
        runTimely4000ms = () -> {
            synchronized (competitionOrder) {
                competitionOrder.add(runTimely4000ms);
            }
        };
    }

    @Test
    @DisplayName("Shows in what order runnables must be executed depending on their timeout.")
    public void runningTimelyTasksShouldResultInCorrectExecutionOrder() throws InterruptedException {
        var submitter = new Submit(2);
        submitter.submit(run3000ms);
        submitter.submit(run500ms);
        submitter.submit(runTimely1000ms, 1000);
        submitter.submit(runTimely4000ms, 4000);
        submitter.submit(runTimely2000ms, 2000);

        Thread.sleep(10000);

        assertEquals(Arrays.asList(run500ms, runTimely1000ms, runTimely2000ms, run3000ms, runTimely4000ms), competitionOrder,
                "Wrong completion order.");
        submitter.shutdown();
    }
}
