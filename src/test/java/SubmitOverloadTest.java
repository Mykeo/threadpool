import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SubmitOverloadTest {

    @Test
    @DisplayName("Runs increment multiple times that result in correct count.")
    public void multipleRunsByManyProcessesShouldResultInSameValue() throws InterruptedException {
        var submitter = new Submit(5);

        var superCount = new BoxedValue("superCount", 0);
        Runnable increment = () -> {
            synchronized (superCount) {
                superCount.increase();
            }
        };

        int carry = 0;
        int superLimit = 1000000;
        while (carry < superLimit) {
            submitter.submit(increment);
            carry++;
        }

        Thread.sleep(1000);
        submitter.shutdown();

        assertEquals(superCount.read(), 1000000,
                "The incrementation using several threads produced wrong result.");
    }


    @Test
    @DisplayName("Several threads complete more tasks.")
    public void moreTaskShouldBeCompletedInParallel() throws InterruptedException {
        var submitter = new Submit(5);

        var tasksCompleted = new BoxedValue("tasksCompleted", 0);

        // Each task roughly takes 1 millisecond.
        Runnable millisecondRun = () -> {
            try {
                Thread.sleep(1);
            } catch (InterruptedException ignored) { }
            synchronized (tasksCompleted) {
                tasksCompleted.increase();
            }
        };

        int carry = 0;
        int superLimit = 10000;
        while (carry < superLimit) {
            submitter.submit(millisecondRun);
            carry++;
        }

        Thread.sleep(1000);
        submitter.shutdown();

        // How many tasks a thread cannot complete on its own.
        int taskLimit = 1500;
        assertTrue(tasksCompleted.read() >= taskLimit,
                "The threshold has not been met. More tasks must be completed.");
    }

}
