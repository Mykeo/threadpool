import org.junit.jupiter.api.*;


import java.util.PriorityQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class ProcessTest {

    private static volatile boolean processHasBeenRunImmediately = false;
    private static volatile boolean processHasBeenRunEventually = false;


    @Test
    @DisplayName("Run process that has nothing to run.")
    public void runningThreadShouldMakeProcessWait() throws InterruptedException {
        PriorityQueue<TimelyRunnable> submissions = new PriorityQueue<>(TimelyRunnable::compareTo);

        Process process = new Process(submissions);
        Thread thread = new Thread(process);
        thread.start();

        Thread.sleep(100);

        assertEquals(thread.getState(), Thread.State.WAITING, "The thread must be waiting.");

        thread.interrupt();
    }
    
    @Test
    @DisplayName("Runnable without timeout must be run immediately.")
    public void runningThreadShouldMakeProcessRunImmediately() throws InterruptedException {
        var mockRunnable = new TimelyRunnable(System.currentTimeMillis(), () -> processHasBeenRunImmediately = true);

        PriorityQueue<TimelyRunnable> submissions = new PriorityQueue<>(TimelyRunnable::compareTo);
        submissions.offer(mockRunnable);

        Process process = new Process(submissions);
        Thread thread = new Thread(process);
        thread.start();

        Thread.sleep(100);

        assertTrue(processHasBeenRunImmediately, "The runnable with timeout has not been run.");

        thread.interrupt();
    }

    @Test
    @DisplayName("Runnable with timeout must be run eventually.")
    public void runningThreadShouldMakeProcessRunEventually() throws InterruptedException {

        var timeout = 100;
        var mockRunnable = new TimelyRunnable(
                System.currentTimeMillis() + timeout,
                () -> processHasBeenRunEventually = true);

        PriorityQueue<TimelyRunnable> submissions = new PriorityQueue<>(TimelyRunnable::compareTo);
        submissions.offer(mockRunnable);

        Process process = new Process(submissions);
        Thread thread = new Thread(process);
        thread.start();

        Thread.sleep(200);

        assertTrue(processHasBeenRunEventually, "The runnable with timeout has not been run.");

        thread.interrupt();
    }

    @Test
    @DisplayName("Process must consume as many runnables as possible.")
    public void runningProcessFedByInfiniteStreamShouldResultInMultipleRunsOfTheMockRunnable() throws InterruptedException {

        var mockRunnable = new TimelyRunnable(System.currentTimeMillis(), () -> { });

        PriorityQueue<TimelyRunnable> submissions = mock(PriorityQueue.class);
        when(submissions.isEmpty()).thenReturn(false);
        when(submissions.peek()).thenReturn(mockRunnable);
        when(submissions.poll()).thenReturn(mockRunnable);

        Process process = new Process(submissions);
        Thread thread = new Thread(process);
        thread.start();

        Thread.sleep(100);

        thread.interrupt();
        verify(submissions, atLeast(2)).poll();
    }
}
