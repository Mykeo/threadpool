
/**
 * Keeps an integer value shared in a thread-safe way.
 */
public class BoxedValue {

    private final String name;
    private volatile int value;

    /**
     * Creates instance of BoxedValue.
     * @param name A name.
     * @param value A value.
     */
    public BoxedValue(String name, int value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Increments the value in thread-safe manner.
     */
    public synchronized void increase() {
        value++;
    }

    /**
     * Reads the value in thread-safe manner.
     * @return Current value.
     */
    public synchronized int read() {
        return value;
    }

    /**
     * Convert BoxedValue into a string.
     * @return String representation of BoxedValue.
     */
    @Override
    public String toString() {
        return "BoxedValue(" +
                '\'' + name + '\'' +
                ", " + read() +
                ')';
    }
}
